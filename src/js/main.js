import '../css/main.scss';
import Editor from '../../lib/mc-editor';
// 实例代码
let myEdit = new Editor(document.querySelector('textarea'), { mode: 1, content: '这是一款为PROCMS而生的富文本编辑器' });
document.querySelector('#submit').onclick = () => {
  document.querySelector('.content').innerHTML = myEdit.state.data;
};
