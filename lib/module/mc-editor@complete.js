export const undo = {
  name: 'undo',
  icon: '<path d="M491 66c-77 0-149 19-212 53l-56-83L85 332l324-21-67-99c50-24 106-38 166-34 176 9 317 157 317 333 0 185-150 336-335 336-92 0-174-38-235-97-22-22-57-21-78 1-22 21-22 57 0 79 81 79 192 128 314 128 246 0 446-200 446-446S737 66 491 66z" p-id="14605"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const redo = {
  name: 'redo',
  icon: '<path d="M532 66c76 0 148 19 212 53l55-83 138 296-324-21 67-99c-50-24-106-38-166-34-176 9-316 157-316 333-1 185 149 336 334 336 92 0 174-38 235-97 22-22 57-21 78 1 22 21 23 57 0 79a444 444 0 0 1-313 128C286 958 86 758 86 512 85 266 285 66 532 66z" p-id="14818"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const bold = {
  name: 'bold',
  icon: '<path d="M655 484c86 16 149 43 192 82s64 90 64 152c0 71-27 126-82 167-54 40-145 60-272 60H103v-71c45 0 76-7 91-21 14-14 22-43 22-85V260c0-41-7-69-21-84s-45-22-92-22V83l333-10h47c150 0 253 19 307 56 55 37 82 88 82 153 0 48-18 89-54 123s-90 60-163 79z m-212-37h44c47 0 82-11 105-33s34-60 34-114c0-40-7-72-21-96-15-23-34-38-58-45-23-7-58-11-104-11v299z m0 78v246c0 24 2 42 7 55 5 12 15 22 29 31 14 8 34 12 61 12 40 0 70-14 91-43 20-28 30-71 30-128 0-46-7-82-20-107-13-26-31-43-54-52s-57-14-103-14h-41z" p-id="15031"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<b></b>', display: 'inline' });
    }
  }
};
export const italic = {
  name: 'italic',
  icon: '<path d="M684 198c22-82 60-97 97-97h52c8 0 15-7 15-7l8-23c0-7 0-15-15-15-23 0-82 8-202 8-127-8-179-8-194-8s-15 0-15 8l-8 30c0 7 0 7 15 7h68c22 0 44 23 29 90L363 818c-23 67-23 97-105 97h-52c-8 0-8 0-8 8l-7 30c0 7 0 15 7 15 15 0 60-8 180-8 149 0 224 8 239 8s15 0 15-8l7-30c0-7 0-15-7-15h-68c-45 0-67-30-52-89l172-628z" p-id="15250"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<i></i>' });
    }
  }
};
export const strike = {
  name: 'strike',
  icon: '<path d="M646 436l-75-36c-80-36-130-61-153-76-30-20-52-40-65-60-10-15-15-33-15-52 0-30 14-58 42-81s66-36 114-36c62 0 119 23 173 67 53 45 86 104 100 177h29l-8-293h-21c-6 27-12 44-19 51-7 6-17 10-29 10-13 0-35-7-67-21s-59-24-81-29c-29-7-59-11-90-11-85 0-155 25-211 74-56 50-84 109-84 179 0 42 10 79 30 113a280 280 0 0 0 16 24H68v63h229v1h452a2 2 0 0 1 0-1h207v-63H646zM399 561c27 14 58 28 93 44 62 28 105 51 129 69 24 17 43 35 55 55 12 19 18 39 18 60 0 35-15 66-47 92s-74 39-126 39c-68 0-131-24-190-73s-99-120-120-213h-26v331h26c9-17 20-30 33-39s26-13 39-13c14 0 33 4 56 13 40 16 73 26 100 32 27 5 56 8 85 8 97 0 176-26 238-79s93-116 93-189c0-51-14-97-44-137H399z" p-id="15463"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<s></s>' });
    }
  }
};
export const underline = {
  name: 'underline',
  icon: '<path d="M88 95h365v51l-80 8-22 14v373c0 73 15 125 51 154 30 29 88 44 161 44 66 0 117-15 146-51 30-30 44-88 44-161V176l-22-22-80-8V95h285v51l-80 8-15 14v366c0 95-29 168-80 212-52 44-139 66-256 66-59 0-117-7-161-22s-81-37-110-66c-15-15-29-36-37-66-7-29-14-73-14-124V168l-15-14-80-8V95z m804 892H124c-22 0-36-14-36-36s14-37 36-37h768c22 0 37 15 37 37 0 14-15 36-37 36z" p-id="15676"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<u></u>' });
    }
  }
};
export const color = {
  name: 'color',
  icon: '<path d="M800 305L512 11 224 305C67 468 67 731 224 894c81 75 181 119 288 119s207-37 288-119c157-163 157-426 0-589zM512 907c-81 0-157-32-213-94-56-57-88-138-88-220s32-162 88-219l213-219v752z" p-id="15889"></path>',
  menu: `<div class="mc-editor-menu menu-color">
  <input type="text" value="#000000" />
    <ul>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#000000"></li>
      <li co="#000" style="background-color:#6f359e"></li>
      <li co="#000" style="background-color:#4674c1"></li>
      <li co="#000" style="background-color:#eb7d3c"></li>
      <li co="#000" style="background-color:#fdbf2d"></li>
      <li co="#000" style="background-color:#5e9cd3"></li>
      <li co="#000" style="background-color:#72ac4d"></li>

      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>

      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>

      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>

      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>

      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>

      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>
      <li co="#000" style="background-color:#fff"></li>

      </ul>
  </div>`,
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<span style="color:#ff0000"></span>' });
    }
  }
};
export const background = {
  name: 'background',
  icon: '<path d="M980 447L722 190l73-74c16-17 14-43-3-58-15-15-40-15-57 0l-73 73-51-50c-45-45-126-45-172 0L134 387c-36 34-45 84-28 129l-6 10c-15 30-30 58-43 90-11 28-22 56-30 86-15 53-19 100-7 138 15 52 58 82 120 82s105-30 118-82c10-38 6-83-9-138l-13-39 263 265c24 24 52 36 88 36 32 0 58-12 84-32l4-4 306-308c47-49 47-130-1-173z m-61 116l-3 2s-100 94-299 0c-190-90-346-3-359 4l-66-66c-15-15-15-41 2-58l303-306c13-15 45-15 58 0l49 49-119 120c-15 17-15 43 2 58s42 15 59 0l118-118 257 257c15 13 15 40-2 58z" p-id="16096"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ css: '<span style="background-color:#ff9900"></span>' });
    }
  }
};
export const eraser = {
  name: 'eraser',
  icon: '<path d="M863 71L402 57c-51 0-95 29-109 80L95 766c-15 51-15 102 15 146 7 15 22 30 36 44 22 15 51 22 73 22h352c87 0 146-36 168-95l205-658c14-73-15-139-81-154zM688 846c-15 44-52 66-117 66H227c-30 0-59-14-66-36-22-30-22-66-15-103l88-263h563L688 846z" p-id="16315"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const title = {
  name: 'title',
  icon: '<path d="M883 95v43q0 17-11 35t-24 19h-32q-15 4-18 18-2 7-2 38v670q0 15-11 25t-25 11h-62q-15 0-25-11t-11-25V209h-83v709q0 15-10 25t-26 11h-62q-16 0-26-11t-10-25V629q-85-7-142-34-74-34-112-104-37-68-37-151 0-96 51-166 51-69 121-93 65-21 243-21h279q14 0 25 10t10 25z" p-id="16522"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const omega = {
  name: 'omega',
  icon: '<path d="M641 962v-2c-29-8-51-34-51-65V749c0-25 15-48 38-59 94-44 155-138 155-240 0-147-121-266-270-266-148 0-268 119-268 266 0 102 60 196 154 240 23 11 38 34 38 59v146c0 37-30 67-68 67H126c-37 0-68-30-68-67s30-66 68-66h177v-41l-1-1c-51-30-95-73-128-123-42-64-65-139-65-216 0-219 182-398 405-398 222 0 404 179 404 398 0 77-22 152-65 216-32 50-76 92-127 123l-1 1v41h177c36 0 67 29 67 66s-30 67-67 67H641z" p-id="16741"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const quote = {
  name: 'quote',
  icon: '<path d="M216 937c-69-15-122-76-138-137-7-31-7-69 0-100 0-7 184-519 214-611h100L231 555c15 0 31-7 38-7 107 0 199 91 199 198 0 123-122 222-252 191z m481 0c-69-15-122-68-137-137-8-31-8-61 0-92 0-15 183-527 221-626h100L705 555c15 0 30-7 38-7 107 0 199 91 199 198 0 123-115 222-245 191z" p-id="16954"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<blockquote></blockquote>' });
    }
  }
};
export const line = {
  name: 'line',
  icon: '<path d="M998 578H24c-13 0-24-11-24-25v-22c0-13 11-24 24-24h974c14 0 24 11 24 24v22c0 14-10 25-24 25z" p-id="17167"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const ol = {
  name: 'ol',
  icon: '<path d="M360 800h592v96H360z m0-336h592v96H360z m0-336h592v96H360zM144 272h48V88H112l-16 48h48z m88 280h-56l24-24c24-24 32-40 32-56 0-32-32-64-72-64-48 0-80 24-80 72h48c0-16 8-32 32-32 16 0 24 8 24 24l-32 40-72 56v32h152v-48z m-16 288l-16-8c16-8 24-24 24-40l-8-16-8-16-24-16h-24c-24 0-40 0-48 16-16 8-24 24-24 40h48l8-16h24l8 16-8 16-16 8h-8v32h8l24 8 8 16-8 16-16 8-16-8-8-16h-56l8 24 16 24 24 16h64l16-16 16-24 8-24-8-24-8-16z" p-id="12605"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<ol></ol>' });
    }
  }
};
export const ul = {
  name: 'ul',
  icon: '<path d="M360 800h592v96H360z m0-336h592v96H360z m0-336h592v96H360z m-280-16h128v128H80z m0 336h128v128H80z m0 336h128v128H80z" p-id="12392"></path>',
  on: {
    mousedown: (ev, _this) => {
      _this.editor({ tag: '<ul></ul>' });
    }
  }
};
export const dedent = {
  name: 'dedent',
  icon: '<path d="M76.8 800h870.4v96H76.8v-96zm345.6-448h524.8v96H422.4v-96zM76.8 128h870.4v96H76.8v-96zm345.6 448h524.8v96H422.4v-96zM288 665.6L76.8 512l204.8-153.6v307.2z"/>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const indent = {
  name: 'indent',
  icon: '<path d="M80 800h872v96H80z m344-448h528v96H424zM80 128h872v96H80z m344 448h528v96H424zM80 360l208 152-208 152z" p-id="12292"/>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const lineHeight = {
  name: 'lineHeight',
  icon: '<path d="M448 808h504v96H448zM304 584h648v96H304z m144-448h504v96H448zM304 360h648v96H304z m-24-80L168 88 48 280h80v480H48l120 192 112-192h-72V280z" p-id="13255"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const left = {
  name: 'left',
  icon: '<path d="M80 792h872v96H80z m0-224h688v96H80z m0-448h624v96H80z m0 224h872v96H80z" p-id="12409"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const center = {
  name: 'center',
  icon: '<path d="M328 120h376v96H328zM80 344h872v96H80z m184 216h512v96H264zM80 784h872v96H80z" p-id="12622"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const right = {
  name: 'right',
  icon: '<path d="M328 120h624v96H328zM80 344h872v96H80z m192 224h688v96H272zM80 792h872v96H80z" p-id="12835"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const justify = {
  name: 'justify',
  icon: '<path d="M80 808h872v96H80z m0-672h872v96H80z m0 224h872v96H80z m0 224h872v96H80z" p-id="12180"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const image = {
  name: 'image',
  icon: '<path d="M936 104H168c-56 0-96 48-96 104v608c0 56 40 104 96 104h768c48 0 88-48 88-104V208c8-56-32-104-88-104z m-8 712v8H176l-8-8V208l8-8h752v616z" p-id="12405"></path><path d="M448 560l-56-64-136 136v80h576V528L664 352zM336 432c24 0 40 0 56-24 16-16 24-32 24-56s0-40-24-56c-16-16-32-24-56-24s-40 8-56 24-24 32-24 56 8 40 24 56c16 24 32 24 56 24z" p-id="12406"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      alert('调起面板');
    }
  }
};
export const file = {
  name: 'file',
  icon: '<path d="M888 256h-48v-56c0-48-40-96-96-96H128c-48 0-96 48-96 96v624c0 48 48 96 96 96h760c48 0 96-48 96-96V352c0-48-40-96-96-96zM312 360c-56 0-112 56-112 112v368h-72l-16-16V200l16-16h616l8 8 8 8v56H576c-24 0-40 8-56 24L432 360H312z m176 360l136-216 144 216H488z" p-id="12390"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      alert('调起面板');
    }
  }
};
export const video = {
  name: 'video',
  icon: '<path d="M896 112H128C72 112 32 160 32 216v608c0 56 40 104 96 104h768c48 0 88-48 88-104V216c8-56-32-104-88-104z m-688 728H104V704h104v136z m0-256H104V448h104v136z m0-248H104V200h104v136z m544 520H272V184h480v672z m168-16h-104V704h104v136z m0-256h-104V448h104v136z m0-248h-104V200h104v136z" p-id="12349"></path><path d="M368 336v368l304-184z" p-id="12350"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      alert('调起面板');
    }
  }
};
export const table = {
  name: 'table',
  icon: '<path d="M888 104H128c-48 0-96 48-96 96v624c0 48 48 96 96 96h760c48 0 96-48 96-96V200c0-48-40-96-96-96zM400 256h224v144H400V256z m0 216h224v152H400V472z m-72 368H112V696h216v144z m0-216H112V472h216v152z m0-224H112V256h216v144z m72 440V696h224v144H400z m512 0H696V696h216v144z m0-216H696V472h216v152z m0-224H696V256h216v144z" p-id="12362"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      alert('调起菜单');
    }
  }
};
export const code = {
  name: 'code',
  icon: '<path d="M863 110H161c-66 0-117 51-117 117v578c0 65 51 117 117 117h702c66 0 117-52 117-117V227c0-66-51-117-117-117z m-388 73c22 0 37 22 37 44s-15 36-37 36-43-14-43-36 21-44 43-44z m-160 0c21 0 36 14 36 36s-15 44-36 44-37-14-37-36 15-44 37-44z m-154 0c22 0 36 14 36 36s-14 44-36 44-37-14-37-36 15-44 37-44z m739 614c0 22-15 37-37 37H161c-22 0-37-15-37-37V380c0-22 15-36 37-36h702c22 0 37 14 37 36v417z" p-id="15177"></path><path d="M241 432l154 153-154 154h117l154-147-154-160H241z m271 234h234v80H512v-80z" p-id="15178"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const link = {
  name: 'link',
  icon: '<path d="M944 632l-144-144c-56-64-152-64-208 0l-16 8-48-48 16-16c56-56 56-152 0-208L400 80c-64-56-152-56-208 0L80 192c-56 56-56 144 0 208l144 144c56 56 152 56 208 0l16-16 48 48-8 16c-64 56-64 152 0 208l136 144c64 56 152 56 208 0l112-112c56-56 56-144 0-200zM368 448c-24 24-56 24-80 0L176 336c-24-24-24-64 0-88l80-72c16-24 56-24 80 0l112 112c24 24 24 64 0 80l-64-64-80 80 64 64z m480 320l-80 80c-24 24-56 24-80 0L576 736c-24-24-24-64 0-88l64 72 80-80-64-64c16-24 56-24 80 0l112 112c24 24 24 56 0 80z" p-id="12418"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const map = {
  name: 'map',
  icon: '<path d="M698 123c-48-47-113-73-181-73s-132 26-181 73c-50 49-78 114-77 184 0 1 0 14 3 35 17 99 95 214 142 274l5 6c39 48 79 79 80 81l28 22 28-23c1-1 41-33 81-81l4-7c57-73 127-180 143-273 3-21 3-34 3-35 0-69-27-134-78-183zM517 443c-64 0-117-51-117-114s53-114 117-114 117 51 117 114-52 114-117 114z" p-id="15598"></path><path d="M843 990H226c-21 0-38-18-38-39s17-38 38-38h617c23 0 42-19 42-42s-19-41-42-41H174c-65 0-119-53-119-118v-2c0-65 54-119 119-119h91c21 0 38 18 38 39s-17 38-38 38h-91c-23 0-42 19-42 42v2c0 23 19 41 42 41h669c65 0 119 53 119 118s-54 119-119 119z" p-id="15599"></path>',
  on: {
    mousedown: (ev) => {
      ev.preventDefault;
      console.log('code');
    }
  }
};
export const full = {
  name: 'full',
  icon: '<path d="M654 926c-10 0-16-5-21-15-4-9-2-16 5-23l89-89-215-217-216 216 89 89c8 7 10 15 6 24-5 10-11 14-21 14H121c-6 0-12-2-16-6s-7-10-7-16V654c0-11 4-17 15-21l9-3c3 0 8 1 15 7l89 89 216-214-216-216-90 89c-4 5-10 7-15 7l-9-2c-10-4-14-10-14-20V121c0-6 2-12 7-16s10-7 16-7h249c10 0 16 4 21 15 4 9 2 16-5 23l-90 89 216 217 216-216-89-89c-8-7-10-15-6-24 5-10 11-14 21-14h249c6 0 12 2 16 6s7 10 7 16v249c0 11-4 17-14 21-3 2-6 2-9 2-5 0-11-2-15-6l-89-89-217 214 216 216 89-89c6-7 11-8 15-8l10 3c9 4 14 10 14 21v249c0 6-2 11-7 16s-10 6-16 6H654z" p-id="15806"></path>',
  on: {
    mousedown: (ev, _this) => {
      ev.preventDefault;
      _this.el.editor.setAttribute('data-view', 'full');
    }
  }
};
export const page = {
  name: 'page',
  icon: '<path d="M512 549v256q0 14-11 25t-26 11-25-11l-83-82-189 190q-6 5-13 5t-14-5l-65-65q-5-6-5-14t5-13l190-189-82-83q-11-11-11-25t11-26 25-11h256q15 0 26 11t11 26z m431-384q0 7-5 13L748 367l82 83q11 11 11 25t-11 26-25 11H549q-15 0-26-11t-11-26V219q0-14 11-25t26-11 25 11l83 82L846 86q6-5 13-5t14 5l65 65q5 6 5 14z" p-id="16013"></path>',
  on: {
    mousedown: (ev, _this) => {
      ev.preventDefault;
      _this.el.editor.setAttribute('data-view', 'page');
    }
  }
};

let modules = [undo, redo, '|', color, background, bold, italic, strike, underline, eraser, '|', title, quote, omega, line, '|', ol, ul, '|', indent, dedent, lineHeight, '|', left, center, right, justify, '|', image, file, video, table, code, link, map, '|', full, page];
module.exports = modules;
