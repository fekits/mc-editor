// const complete = require('./module/mc-editor@complete');
import complete from './module/mc-editor@complete';

// 转换DOM
let ment = (tag) => {
  return tag ? document.createElement(tag) : document.createDocumentFragment();
};
let html = (str) => {
  if (typeof str == 'string') {
    let div = ment('div');
    div.innerHTML = str;
    return div.children[0];
  }
};
// 遍历出选区内所有的最小单位节点
let lest = (node) => {
  let lests = [];
  let s = window.getSelection ? window.getSelection() : document.selection.createRange();
  // 获取选中的所有最小单位节点
  let _lest = (node) => {
    let _sub = node.childNodes,
      _len = _sub && _sub.length;
    if (_len) {
      for (let i = 0; i < _len; i++) {
        _lest(_sub[i]);
      }
    } else {
      if (s.containsNode(node, false)) {
        lests.push(node);
      }
    }
  };
  _lest(node);
  return lests;
};

const isinline = (node) => {
  return node.nodeType === 1 && /^span|b|em|i|u|s|q|br|font|var|sub|kbd|strong|strike|sup|code|small|big|abbr$/gi.test(node.tagName);
};

// 拼接SVG
const svg = (path) => {
  return '<svg viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="14" height="14">' + path + '</svg>';
};
// 最大范围
const getMaxRangeNodes = () => {};
// 最小范围
const getMinRangeNodes = () => {};

class McEditor {
  constructor(source, param = {}) {
    if (source) {
      let { content, mode = 1, prefix = 'mc-editor', modules = complete } = param;

      // 节点
      this.el = { source };
      // 模式
      this.mode = mode;
      // 图标
      this.icons = Object.assign([], modules);

      // 状态
      this.state = {
        data: content ? content : '<p><br></p>',
        path: [],
        warn: {
          icon: '<path d="M590 99l385 641a91 91 0 0 1-78 138H127a91 91 0 0 1-77-138L434 99a91 91 0 0 1 156 0z m-78 143a45 45 0 0 0-45 45v273a45 45 0 0 0 91 0V287a45 45 0 0 0-46-45z m0 545a45 45 0 1 0 0-91 45 45 0 0 0 0 91z" p-id="16226"></path>',
          list: ['']
        }
      };

      this.free = 1;

      // 隐藏原节点
      source.style.display = 'none';

      // 工具栏
      let tools = html('<ul class="' + prefix + '-tools">');

      console.dir(window);

      // 图标
      this.el.icons = this.icons.map((item) => {
        let li = '';
        let { name, icon, menu = '', text, on } = item;
        // 生成DOM
        if (name) {
          li = `<li data-work="${on ? 1 : 0}" class="${prefix}-${name}"><span>${text ? text : svg(icon)}</span>${menu}</li>`;
        } else {
          li = '<li class="' + prefix + '-separator">' + item + '</li>';
        }
        let dom = html(li);
        // 图标事件
        if (on) {
          for (let key in on) {
            if ({}.hasOwnProperty.call(on, key)) {
              dom.addEventListener(key, (e) => {
                e.preventDefault();
                on[key](e, this);
              });
            }
          }
        }
        tools.appendChild(dom);
        return dom;
      });

      // 辅助线
      let el_guide = html('<div class="' + prefix + '-guide"></div>');

      // 内容区
      let el_content = html('<div class="mc-editor-content" contenteditable="true">' + this.state.data + '</div>');

      let append = (p, c) => {
        c = Array.isArray(c) ? c : [c];
        c.map((i) => {
          p.appendChild(i);
        });
      };

      // 内容区
      let el_wraps = html('<div class="' + prefix + '-wraps"></div>');

      // 弹框区
      // let el_mode = html('<div class="mc-editer-mode" view="0"><div><div><div><span>AUTO</span><span>HTML</span></div></div></div></div>');
      // document.querySelector('');

      append(el_wraps, [el_content, el_guide]);
      // append(el_wraps, el_guide);

      // 路径
      let paths = html('<ul class="' + prefix + '-state-path"><li>root</li></ul>');

      // 警告
      let warnNum = html('<span>0</span>');
      let warn = html('<li>' + svg(this.state.warn.icon) + ' </li>');
      // let setting = html('<li>' + svg(this.state.warn.icon) + ' </li>');
      append(warn, warnNum);
      let status = html('<ul class="' + prefix + '-state-warn"></ul>');
      append(status, warn);
      // 状态s栏
      let state = html('<div class="' + prefix + '-state"></div>');
      append(state, [paths, status]);
      // append(state, status);

      // 生成辑器框
      let editor = html('<div class="' + prefix + '"></div>');
      append(editor, [tools, el_wraps, state]);
      // 获取原节点的父级
      let parent = source.parentNode;
      if (parent) {
        // 在原节点前面插入插件DOM
        parent.insertBefore(editor, source);

        // 插入初始标签
        el_content.innerHTML = this.state.data;

        document.addEventListener('selectionchange', (ev) => {
          this.__selects(ev);
        });

        el_content.addEventListener('mousedown', () => {
          this._is_content_div = 1;
        });

        el_content.addEventListener('mouseout', () => {
          this._is_content_div = 0;
        });

        el_content.addEventListener('mouseup', () => {
          this.__cleanup();
        });

        // 监听按键
        // el_content.addEventListener('keyup', (ev) => {
        //   ev.preventDefault();
        //   if (ev.keyCode === 13) {
        //     let { s, r } = this.__selects();
        //     let _del = (node) => {
        //       if (node.parentNode.textContent !== '') {
        //         node.innerHTML = '<br>';
        //         s.setBaseAndExtent(node, 1, node, 1);
        //       } else {
        //         _del(node.parentNode);
        //       }
        //     };
        //     if (r.commonAncestorContainer.textContent === '') {
        //       console.dir(r.commonAncestorContainer);
        //       _del(r.commonAncestorContainer);
        //     }
        //   }
        //   this.__paths(ev);
        //   this.data();
        // });

        // 监听焦点
        el_content.addEventListener('blur', () => {
          this.data();
        });

        // 监听粘帖
        el_content.addEventListener('paste', (e) => {
          e.preventDefault();
          if (e.clipboardData) {
            // 谷歌火狐
            for (let i = 0, len = e.clipboardData.items.length; i < len; i++) {
              let item = e.clipboardData.items[i];
              if (item.kind === 'string') {
                if (this.mode === 1) {
                  // 智能模式
                  if (item.type === 'text/html') {
                    item.getAsString((code) => {
                      code = this.__filter(code);
                      this.inset(code);
                    });
                  }
                } else {
                  // 文本模式
                  if (item.type === 'text/plain') {
                    item.getAsString((code) => {
                      code = (code + '\n').replace(/([^\n]*)(\n)/g, (res, s1) => {
                        return s1 + '<br>';
                      });
                      this.inset(code);
                    });
                  }
                }
              } else if (item.kind === 'file') {
                let file = item.getAsFile();
                this.inset(file);
              }
            }
          } else if (window.clipboardData) {
            // ie9及其上
            let code = window.clipboardData.getData('Text');
            code = this.__filter(code);
            this.inset(code);
          } else {
            // console.log('浏览器版本过低');
          }
        });
      }

      this.el = Object.assign(this.el, {
        source,
        editor,
        tools,
        wraps: el_wraps,
        content: el_content,
        guide: el_guide,
        state,
        parent,
        paths,
        status,
        warn
      });

      this.__paths();
    }
  }

  // 过滤内容
  __filter(code) {
    // 清理所有属性
    code = code.replace(/(\s[\w-]*)(=)((["'])([^\4]*?)(\4))/g, (S0, S1) => {
      S1 = S1.replace(/\s*/gi, '');
      // 忽略清理列表
      return /^href|target|src|alt|colspan|rowspan|input|textarea$/gi.test(S1) ? S0 : '';
    });

    // 清理指定标签
    const root = html(`<div>${code}</div>`);
    const nodes = root.getElementsByTagName('*');
    // 遍历每个标签
    for (let i = 0; i < nodes.length; i++) {
      const node = nodes[i];
      // 清除以下标签
      if (/^form|link|head|meta|script|style|input|textarea$/gi.test(node.tagName)) {
        node.remove();
        i--;
      }
    }
    code = root.innerHTML;
    return code;
  }

  // 插入内容
  inset(str) {
    let node = html('<div>' + str + '</div>');
    this.r.insertNode(node);
    let _end = node.nodeName === '#text' ? node.textContent.length : node.childNodes.length;
    this.s.setBaseAndExtent(node, _end, node, _end);
    this.__cleanup(node);
    this.data();
  }

  // 返回内容
  data() {
    this.state.data = this.el.content.innerHTML;
    this.el.source.value = this.state.data;
    return this.state.data;
  }

  // 获取路径
  __paths(ev) {
    // console.log(ev, this.s);
    let {
      state: { path },
      el: { content, paths }
    } = this;
    // 遍历路径
    let PN = (focus) => {
      if (focus && focus !== content) {
        if (focus.nodeName) {
          path.unshift({
            // name: focus.nodeName.toUpperCase(),
            name: focus.nodeName.toLowerCase(),
            el: focus
          });
        }
        PN(focus.parentNode);
      }
    };

    path = [];
    if (ev && ev.target && ev.target.nodeName === 'IMG') {
      // 点击图片
      let focus = ev.target;
      PN(focus);
    } else {
      // 进入编辑
      if (this.s && this.s.focusNode) {
        PN(this.s.focusNode);
      }
    }
    // 插入根路径
    path.unshift({
      icon: '<path d="M51 154h103V51C97 51 51 97 51 154z m0 409h103V461H51v102z m205 410h102V870H256v103zM51 358h103V256H51v102zM563 51H461v103h102V51z m307 0v103h103c0-57-46-103-103-103zM154 973V870H51c0 57 46 103 103 103zM51 768h103V666H51v102zM358 51H256v103h102V51z m103 922h102V870H461v103z m409-410h103V461H870v102z m0 410c57 0 103-46 103-103H870v103z m0-615h103V256H870v102z m0 410h103V666H870v102zM666 973h102V870H666v103z m0-819h102V51H666v103zM256 768h512V256H256v512z m102-410h308v308H358V358z" p-id="12152"></path>',
      el: content
    });

    paths.innerHTML = '';
    path.map((item) => {
      let dom = html(`<li>${item.name || svg(item.icon)}</li>`);
      let set = () => {
        let _offset = item.el.nodeName === '#text' ? item.el.textContent.length : item.el.childNodes.length;
        this.s.setBaseAndExtent(item.el, 0, item.el, _offset);
      };
      dom.onclick = (ev) => {
        ev.preventDefault();
        set();
        console.log('弹出面板');
      };
      dom.onmouseover = (ev) => {
        ev.preventDefault();
        set();
      };
      paths.appendChild(dom);
    });
  }

  // 编辑内容-内核
  editor(conf) {
    // 是否空闲
    if (this.free) {
      // 非空闲
      this.free = 0;

      let { tag, css, clear = 0 } = conf;

      // 判断是块级还是行内
      console.log(isinline(html(tag)));

      // 当前是否有选中内容
      if (this.s) {
        // 选中内容是否为空
        if (!this.r.collapsed) {
          // 判断是否全部行内元素
          const cac = this.r.commonAncestorContainer;
          const allinline = cac.nodeType === 1 ? Object.values(cac.getElementsByTagName('*')).every((n) => isinline(n)) : 1;
          
          // sel
          
          if (allinline) {
            console.log('行内');
          } else {
            // 获取全部选中节点
            this.lests = this.__getlest();
            // console.log(this.lests, this.sta, this.end);
            // return;

            // 判断是否重复操作
            if (tag) {
              clear = this.__dittoed(tag);
              console.warn('是否重复:' + clear);
            } else {
              // css
            }

            let _tagName = html(tag || css).tagName;
            // 跨标签处理
            let _have_tag = (node) => {
              let _has = 0;
              let _is_tag = (node) => {
                if (node.tagName && node.tagName === _tagName) {
                  _has = 1;
                } else {
                  if (node.parentNode && node !== this.el.content) {
                    _is_tag(node.parentNode);
                  }
                }
              };
              _is_tag(node);
              return _has;
            };

            // 添加标签
            let _tag = (node) => {
              // 判断是否有子元素
              let _sub = node.childNodes,
                _len = _sub && _sub.length;
              if (_len) {
                // 如果有子元素则继续遍历
                for (let i = 0; i < _len; i++) {
                  _tag(_sub[i]);
                }
              } else {
                // 如果无子元素则处理标签
                if (!_have_tag(node)) {
                  let newNode = html(tag);
                  node.parentNode.replaceChild(newNode, node);
                  newNode.appendChild(node);
                }
              }
              return node;
            };

            // 清除标签
            let _delTag = (node) => {
              console.log(442, node);
              let newNode = ment();
              for (let i = 0; node.childNodes.length !== 0; ) {
                newNode.appendChild(node.childNodes[i]);
              }
              node.parentNode.replaceChild(newNode, node);
            };
            let _split = (node, _root) => {
              let _tree_tag = [];
              let _tree_node = [];
              let _tree = (node) => {
                if (node.childNodes && node.childNodes.length) {
                  if (node.tagName) {
                    _tree_tag.push(node.tagName);
                  }
                  let subNodes = node.childNodes;
                  for (let i = 0; i < subNodes.length; i++) {
                    _tree(subNodes[i]);
                  }
                } else {
                  _tree_node.push(node);
                }
              };
              _tree(node);
              if (_tree_node.length <= 1) {
                _delTag(_root);
              } else {
                let _rootNode = ment();
                _tree_node.map((node) => {
                  let _newNode = ment();
                  let _lastNode = _newNode;
                  _tree_tag.map((_tag, index) => {
                    if (!(this.s.containsNode(node, true) && _tag === _tagName)) {
                      let _tagNode = ment(_tag);
                      _lastNode.appendChild(_tagNode);
                      _lastNode = _tagNode;
                    }
                    if (index === _tree_tag.length - 1) {
                      _lastNode.appendChild(node);
                    }
                  });
                  _rootNode.appendChild(_newNode);
                });
                _root.parentNode.replaceChild(_rootNode, _root);
              }
            };
            let _del = (node) => {
              console.log(489, node);
              if (node.tagName && node.tagName === _tagName) {
                if (this.s.containsNode(node, false)) {
                  // console.log(node, '全部选中');
                  _delTag(node);
                } else {
                  // console.log(node, '部份选中');
                  _split(node, node);
                }
              } else {
                if (this.s.containsNode(node.parentNode, true)) {
                  _del(node.parentNode);
                }
              }
            };

            this.lests.forEach((node) => {
              if (clear) {
                _del(node);
              } else {
                _tag(node);
              }
            });
            this.__setarea();
            this.__paths();
          }
        }
      }
      // 设置为空闲
      this.free = 1;
    }
  }
  // 获区对象
  __selects(ev) {
    // 获取获中区域
    let s = window.getSelection ? window.getSelection() : document.selection.createRange();
    let r = s.getRangeAt(0);
    if (this.el.content.contains(s.focusNode)) {
      this.s = s;
      this.r = r;
      if (this._is_content_div) {
        this.__paths(ev);
      }
    }
    return { s, r };
  }

  // 全部末支
  __getlest(nodes = this.el.content) {
    let {
      r: { startContainer: sta, endContainer: end, startOffset: sta_pos, endOffset: end_pos }
    } = this.__selects();
    let sta_is_end = sta === end,
      _sta,
      _sta_pos = 0,
      _end,
      _end_pos;
    if (sta.tagName) {
      let staLests = sta.childNodes;
      let staNode = staLests[sta_pos];
      let _empty_node = document.createTextNode('');
      staNode.parentNode.insertBefore(_empty_node, staNode);
      _sta = _empty_node;
      end_pos++;
    } else {
      if (sta_pos !== 0) {
        _end_pos = end_pos - sta_pos;
        _sta = sta.splitText(this.r.startOffset);
        if (sta_is_end) {
          if (_end_pos < _sta.length) {
            _sta.splitText(_end_pos);
          }
          _end = _sta;
        }
      } else {
        _sta = sta;
        if (sta_is_end) {
          _end = _sta;
          console.log(end_pos);
          _end_pos = end_pos;
          _sta.splitText(_end_pos);
        }
      }
    }

    if (end.tagName) {
      let endLests = end.childNodes;
      let endNode = endLests[end_pos - 1];
      let _empty_node = document.createTextNode('');
      endNode.parentNode.insertBefore(_empty_node, endNode.nextSibling);
      _end = _empty_node;
      _end_pos = 0;
    } else {
      if (!sta_is_end) {
        if (end_pos < end.length) {
          end.splitText(end_pos);
        }
        _end = end;
        _end_pos = _end.textContent.length;
      }
    }
    this.sta = _sta;
    this.end = _end;
    this.sta_pos = _sta_pos;
    this.end_pos = _end_pos;
    this.__setarea();
    return lest(nodes);
  }

  // 全部区块
  __getblock(nodes = this.el.content) {
    console.log(nodes);
  }

  // 重复检测
  __dittoed(tag) {
    let _tagName = html(tag).tagName;
    return this.lests.every((node) => {
      let _has_dittoed = 0;
      // 检测是否重复标签
      let _dittoed = (node) => {
        // 是否为标签
        if (node.tagName && node.tagName === _tagName) {
          _has_dittoed = 1;
        } else {
          if (node.parentNode && node.parentNode !== this.el.content) {
            _dittoed(node.parentNode);
          } else {
            _has_dittoed = 0;
          }
        }
      };
      // 遍历所有末终节点
      let _haschild = (node) => {
        let len = node.childNodes.length;
        if (len) {
          for (let i = 0; i < len; i++) {
            if (this.s.containsNode(node.childNodes[i], true)) {
              _haschild(node.childNodes[i]);
            }
          }
        } else {
          _dittoed(node);
        }
      };
      _haschild(node);
      return _has_dittoed;
    });
  }
  // 设置选区
  __setarea() {
    this.s.setBaseAndExtent(this.sta, this.sta_pos, this.end, this.end_pos);
  }
  // 清理拉圾
  __cleanup(el = this.el.content) {
    let { s } = this.__selects();
    let nodes = el.children;
    let len = nodes.length;
    for (let i = 0; i < len; i++) {
      if (!s.containsNode(nodes[i], true)) {
        nodes[i].normalize();
      }
    }
  }
  // 是否选中
  is_sel(node, a = true) {
    return this.s.containsNode(node, a);
  }
}

export default McEditor;
